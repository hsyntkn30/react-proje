import React, {Component} from 'react';
import Navi from './Navi';
import CategoryList from  './CategoryList';
import ProductList from './ProductList';
import {Container,Row,Col} from 'reactstrap';

export default class App extends Component{
    state = {currentCategory:""}
    changeCategory = (category) => {
        this.setState({currentCategory:category.categoryname})
    }
    render(){
        let titleCategory="Category ";
        let titleProduct="Product "
        return(
            <div>
                <Container>
                    <Row>
                        <Navi/>
                    </Row>
                    <Row>
                        <Col xs="3">
                            <CategoryList currentCategory={this.state.currentCategory} changeCategory={this.changeCategory} title={titleCategory} />
                        </Col>
                        <Col xs="9">
                            <ProductList currentCategory={this.state.currentCategory} title={titleProduct} />
                        </Col>

                    </Row>
                </Container>
            </div>
        )
    }
}
