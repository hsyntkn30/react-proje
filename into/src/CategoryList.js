import React, {Component} from 'react';
import {ListGroup, ListGroupItem} from 'reactstrap';

export default class CategoryList extends Component {
/*    constructor(props){
        super(props);       // Props icin eski surude zorunlu olarak kullanılan bir durum gencel surumlerde this.props.tanımladığımızporps yazabiliriz
        state:{}
    }*/

    //prop bir componenten diğer componenete data ve eventleri tasımamıza yarar.
    //state ilgili datayı ilgili yerde tutup ilgi dosya ıcınde tasımımıza yarar
    constructor(props){
        super(props);}
        state= {category: [
            {categoryid:1,categoryname:"Teknoloji"},
            {categoryid:2,categoryname:"Giyim"}
            ]
        };



    render() {

        return (
            <div>
            <h3>{this.props.title} {this.props.currentCategory}</h3>
            <ListGroup>
                {this.state.category.map(category=>(

                    <ListGroupItem onClick={()=>this.props.changeCategory(category)} key={category.categoryid}>{category.categoryname}</ListGroupItem>
                ))}
            </ListGroup>
                <h4>{this.props.currentCategory}</h4>
            </div>
        );
    }
}
